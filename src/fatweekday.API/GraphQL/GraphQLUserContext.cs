﻿using System.Security.Claims;

namespace fatweekday.API.GraphQL
{
	public class GraphQLUserContext
	{
		public ClaimsPrincipal User { get; set; }
	}
}
