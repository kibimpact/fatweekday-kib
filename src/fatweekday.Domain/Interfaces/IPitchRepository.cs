﻿using fatweekday.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fatweekday.Domain.Interfaces
{
	public interface IPitchRepository
	{
		Pitch GetById(int id);
		IList<Pitch> GetAll();
		Pitch AddPitch(Pitch pitch);
	}
}
