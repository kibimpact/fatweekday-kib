﻿import * as React from 'react';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import PitchItem from './PitchItem';


const GET_PITCH = gql`
	{
		pitches {
			id,
			name,
			effort,
			likeCount
		}
	}
`

const Pitches = () => (
	<Query query={GET_PITCH}>
		{({ loading, error, data }) => {
			if (loading) return <p>Loading...</p>;
			if (error) return <p>Error :(</p>;

			return (
				<div className="app">
					{data.pitches.map((pitch: any, index: number, ) => {
						return <PitchItem id={pitch.id} name={pitch.name} likes={pitch.likeCount} effort={pitch.effort} />
					})}
				</div>
			);
		}}
	</Query>
);

class App extends React.Component {

	public render() {
		var ideas = ['fatweekday portal', 'Impact Slackbot', '3rd idea'];

		return (
			Pitches()
		);
	}
}

export default App;