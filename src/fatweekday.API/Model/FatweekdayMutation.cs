﻿using fatweekday.API.Model.InputTypes;
using fatweekday.API.Model.Types;
using fatweekday.Domain.Interfaces;
using fatweekday.Domain.Models;
using GraphQL.Types;

namespace fatweekday.API.Model
{
	public class FatweekdayMutation : ObjectGraphType
	{
		public FatweekdayMutation(IPitchRepository repo)
		{
			Name = "Mutation";

			Field<PitchType>(
				"createPitch",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<PitchInputType>> { Name = "pitch" }
				),
				resolve: context =>
				{
					var pitch = context.GetArgument<Pitch>("pitch");
					return repo.AddPitch(pitch);
				});
		}
	}
}
