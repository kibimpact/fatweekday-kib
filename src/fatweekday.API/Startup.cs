﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fatweekday.API.GraphQL;
using fatweekday.API.Model;
using fatweekday.API.Model.InputTypes;
using fatweekday.API.Model.Types;
using fatweekday.Domain.Interfaces;
using fatweekday.Infrastructure;
using GraphQL;
using GraphQL.Http;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace fatweekday.API
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
			// Add service and create Policy with options
			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					builder => builder.AllowAnyOrigin()
					.AllowAnyMethod()
					.AllowAnyHeader()
					.AllowCredentials());
			});

			// Repository
			services.AddSingleton<IPitchRepository, PitchRepository>();

			// GraphQL
			services.AddSingleton<IDependencyResolver>(s => new FuncDependencyResolver(s.GetRequiredService));

			services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
			services.AddSingleton<IDocumentWriter, DocumentWriter>();

			services.AddSingleton<FatweekdayMutation>();
			services.AddSingleton<PitchInputType>();
			services.AddSingleton<FatweekdayQuery>();
			services.AddSingleton<PitchType>();
			services.AddSingleton<LikeType>();

			services.AddSingleton<ISchema, FatweekdaySchema>();

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
			app.UseCors("CorsPolicy");

			if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

			app.UseMiddleware<GraphQLMiddleware>(new GraphQLSettings
			{
				BuildUserContext = ctx => new GraphQLUserContext
				{
					User = ctx.User
				}
			});

			app.UseDefaultFiles();
			app.UseStaticFiles();
		}
    }
}
