const path = require('path');
const webpack = require('webpack');

const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;

const bundleOutputDir = './wwwroot/dist';

module.exports = (env) => {
	return {
		mode: 'development',
		devtool: 'source-map',

		stats: { modules: false },
		entry: { 'main': './app/index.tsx' },
		resolve: { extensions: ['.js', '.jsx', '.ts', '.tsx'] },
		output: {
			path: path.join(__dirname, bundleOutputDir),
			filename: '[name].js',
			publicPath: 'dist/'
		},
		module: {
			rules: [
				{ test: /\.tsx?$/, include: /app/, use: 'awesome-typescript-loader?silent=true' },
			]
		},
		plugins: [
			new CheckerPlugin(),
			new webpack.DllReferencePlugin({
				context: __dirname,
				manifest: require('./wwwroot/dist/vendor-manifest.json')
			})
		]
	};
};