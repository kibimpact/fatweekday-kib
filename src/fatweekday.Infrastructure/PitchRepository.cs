﻿using fatweekday.Domain.Interfaces;
using fatweekday.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fatweekday.Infrastructure
{
    public class PitchRepository : IPitchRepository
    {
		IList<Pitch> orderStore = new List<Pitch> {
			new Pitch {
				Id = 1,
				Name = "First Pitch!",
				Effort = 21,
				Likes = new Like[] {
					new Like { PitchId = 1, UserId = 3 },
					new Like { PitchId = 1, UserId = 4 }
				}
			},
			new Pitch {
				Id = 2,
				Name = "My Name!",
				Effort = 80,
				Likes = new Like[] {
					new Like { PitchId = 2, UserId = 3 }
				}
			},
			new Pitch {
				Id = 3,
				Name = "My Name!",
				Effort = 5,
				Likes = new Like[] {
					new Like { PitchId = 3, UserId = 3 },
					new Like { PitchId = 3, UserId = 4 },
					new Like { PitchId = 3, UserId = 5 }
				}
			},
		};

		public Pitch GetById(int id)
		{
			return orderStore.Where(x => x.Id == id).FirstOrDefault();
		}

		public IList<Pitch> GetAll()
		{
			return orderStore;
		}

		public Pitch AddPitch(Pitch pitch) {
			pitch.Id = orderStore.Select(x => x.Id).Max() + 1;

			orderStore.Add(pitch);
			
			return pitch;
		}
	}
}
