﻿import * as React from 'react';

interface IPitchItemProps {
	id: number;
	name: string;
	effort: number;
	likes: number;
}

class App extends React.Component<IPitchItemProps, any> {

	constructor(props: IPitchItemProps) {
		super(props);
	}

	public render() {
		return (
			<div>{this.props.name} has {this.props.likes} likes (effort: {this.props.effort})</div>
		);
	}
}

export default App;