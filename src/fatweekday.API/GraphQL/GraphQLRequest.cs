﻿using Newtonsoft.Json.Linq;

namespace fatweekday.API.GraphQL
{
	public class GraphQLRequest
	{
		public string OperationName { get; set; }
		public string Query { get; set; }
		public JObject Variables { get; set; }
	}
}
