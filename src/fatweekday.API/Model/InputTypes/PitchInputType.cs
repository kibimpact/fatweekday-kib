﻿using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.InputTypes
{
    public class PitchInputType : InputObjectGraphType<Pitch>
	{
		public PitchInputType()
		{
			Name = "PitchInput";
			Field(h => h.Name).Description("The Pitch name");
			Field(h => h.Description, nullable: true).Description("The Pitch description");
			Field(h => h.Effort).Description("The Pitch effort");
		}
	}
}
